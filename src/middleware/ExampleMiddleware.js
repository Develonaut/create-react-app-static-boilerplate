import { EXAMPLE_ACTION } from '../modules/ExampleModule';
import axios from 'axios';

let dispatch = null;
let getState = null;

const ExampleMiddleware = store => {
  dispatch = store.dispatch;
  getState = store.getState;


  return next => action => {
    switch (action.type) {
    case EXAMPLE_ACTION:
      doExample(action);
      break;
    default:
      break;
    }

    return next(action);
  };
};

function doExample(action) {
  // Axios Docs: https://github.com/axios/axios
  const exampleUrl = window.location.href;
  axios.get(exampleUrl)
    .then(res => {
      console.log("ExampleMiddleware - doExample", {
        dispatch,
        reduxStore: getState(),
        action,
      });
    })
    .catch(err => {
      throw err;
    });
}

export default ExampleMiddleware;
