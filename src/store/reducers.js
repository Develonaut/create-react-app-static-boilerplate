import { combineReducers } from 'redux'
import example from '../modules/ExampleModule';

const reducers = combineReducers({
    example,
})

export default reducers