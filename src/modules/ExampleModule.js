import { createSelector } from 'reselect'

// Selectors
const exampleSelector = state => state.example;

export const getExampleString = createSelector(
  exampleSelector,
  example => example,
);

// Actions
export const EXAMPLE_ACTION = 'EXAMPLE_ACTION';

export function exampleAction({ string = '' }) {
  return { type: EXAMPLE_ACTION, string }
}

const initState = {
  example: "Create React App Boilerplate",
};

export default function ExampleModule(state = initState, action) {
    switch (action.type) {
      case EXAMPLE_ACTION:
        return {
          ...state,
          example: action.string,
        }
      default:
        return state
    }
  }