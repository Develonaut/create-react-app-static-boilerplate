import React, { Component } from 'react';
import { connect } from 'react-redux';
import Helmet from 'react-helmet';
import './../stylesheets/App.css';
import { exampleAction, getExampleString } from '../modules/ExampleModule';

class App extends Component {
  componentDidMount() {
    const { exampleAction: dispatchExampleAction } = this.props;
    // Set a delay to show that the reducer state is updating.
    setTimeout(() => {
      dispatchExampleAction({ string: 'Create React App Boilerplate Updated' });
    }, 1000);
  }

  render() {
    const { example } = this.props;
    return (
      <div className="App">
        <Helmet title="Create React App Static Boilerplate" />
        <header className="App-header">
          <h1 className="App-title">{`Welcome to ${example}`}</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    ...getExampleString(state),
  }
}

const mapDispatchToProps = {
  exampleAction,
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
